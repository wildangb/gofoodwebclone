import React from 'react';
import './cover.css';

const Cover = () => {
    return (
        <div className='cover-wrapper'>
            <h2>Cicipin macam-macam resto paling top</h2>
            <h5>Dari kaki lima, yang viral, sampe kuliner legendaris, makanan enak kami antar ke kamu.</h5>
            <a href='https://gofood.co.id/get-app'>
                <button type='button' >Download Gojek</button>
            </a>
        </div>
    )
};
  
export default Cover;