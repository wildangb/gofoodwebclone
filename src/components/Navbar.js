import React, { useEffect } from 'react';
import './navbar.css';

const Navbar=() => {
    const [ scrolled,setScrolled ] = React.useState( false );

    const handleScroll = () => {
        const offset = window.scrollY;
        if( offset > 350 ){
            setScrolled( true );
        }
        else{
            setScrolled( false );
        }
    };

    useEffect( () => {
        window.addEventListener( 'scroll', handleScroll );
    } );

    let navbarClasses=['navbar'];

    if( scrolled ){
        navbarClasses.push('navbar-scrolled');
    }

    return (
    <header className={navbarClasses.join(" ")}>

        <div className="logo">
            <a href='.'>
                <img src="/go-food.svg" alt="profile" />
            </a>
        </div>
        <nav className="navigation">
            <p>Dapetin app Gojek</p>
            <a href='https://gofood.co.id/get-app'>
                <button type='button' >Download</button>
            </a>
        </nav>

    </header> 
    )

};
  
  export default Navbar;