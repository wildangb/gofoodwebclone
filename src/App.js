import React from 'react';
import './app.css';
import Navbar from './components/Navbar'
import Content from './components/Content'

function App() {
  return (
    <div className='App'>
      <Navbar />
      <Content />
    </div>
  );
}

export default App;
